run:
	make debug_run -j2

debug_run : run_flask run_react

run_flask:
	FLASK_ENV=development FLASK_APP=src/app.py flask run

run_react:
	cd ./react_src/ && npm start

build_react:
	cd ./react_src/ && npm run build

run_prod:
	cd ./react_src/ && serve -s build