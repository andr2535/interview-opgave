from flask import render_template
from python_files.products import getAllProducts

def getFrontpage():
    products = getAllProducts()
    name = 'TestShop.dk'
    return render_template('frontpage.html', name=name, products=products)