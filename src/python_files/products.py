from flask import jsonify, json

class Product:
    def __init__(self, name, price, id, imgurl):
        self.name = name
        self.price = price
        self.id = id
        self.imgurl = imgurl
    def to_dict(self):
        return {"name": self.name, "price": self.price, "id": self.id, "self.imgurl": self.imgurl}
class ProductError:
    def __init__(self, errormessage, id):
        self.errormessage = errormessage
        self.id = id

def getProduct(id):
    """ Returns either a Product object or a ProductError object."""
    try:
        id = int(id)
        if id == 10:
            return Product("product10", 100, 10, "/images/10.jpg")
        if id == 12:
            return Product("product12", 120, 12, "/images/12.jpg")
        if id == 14:
            return Product("product14", 140, 14, "/images/14.jpg")
        if id == 15:
            return Product("product15", 150, 15, "/images/15.jpg")
    except:
        return ProductError("Id er ikke et gyldigt heltal", id)
    return ProductError("Intet product har dette id nummer", id)

def getAllProducts():
    # Includes one invalid id number "2". This should be filtered out.
    productIds = [10, 2, 12, 14, 15]
    products = filter(lambda x: type(x) != ProductError, map(lambda x: getProduct(x), productIds))
    return jsonify(list(map(lambda x: x.to_dict(),products)))