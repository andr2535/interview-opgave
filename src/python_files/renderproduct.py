from flask import render_template
from python_files.products import Product, ProductError

def renderProduct(product):
    assert type(product) == Product or type(product) == ProductError

    if type(product) == Product:
        return render_template('productpage.html', product=product, t="tressss")
    else:
        return render_template('productErrorPage.html', productError=product)