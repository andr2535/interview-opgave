// Code for creating products and links to remove products from the cart page.

const productIds = getProductsFromCookie();

Promise.all(productIds.map(productId => fetch("/productJson/" + productId))).then(products => {
    Promise.all(products.map(product => product.json())).then(products => {

        let htmlProductlist = document.getElementById("productlist");

        let index = 0;
        products.forEach(product => {
            let div = document.createElement("div");
            div.appendChild(document.createTextNode(product.name + " med pris: " + product.price));
            
            let a = document.createElement("a");
            a.appendChild(document.createTextNode("Fjern"));
            // Prevents using the final number after the forEach has ended.
            let usedIndex = index;
            a.onclick = () => {
                removeProductFromCart(usedIndex);
                window.location.reload();
            };
            div.appendChild(a);

            htmlProductlist.appendChild(div);
            index++;
        });


    });
});