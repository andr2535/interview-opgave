// Code for manipulating the cart of the shop. Used by both the cart and the product page.
function getCookies() {
    let cookieObject = {};
    document.cookie.split(';').forEach(cookieString => {
        let cookieArray = cookieString.split("=");
        cookieObject[cookieArray[0]] = cookieArray[1];
    });
    return cookieObject;
}
function setCookie(key, value) {
    document.cookie = key + "=" + JSON.stringify(value) + "; Path=/;"
}

function getProductsFromCookie() {
    const cookies = getCookies();
    if (cookies["products"]) {
        return JSON.parse(cookies["products"]);
    }
    else {return [];}
}

function addProductToCart(id) {
    let products = getProductsFromCookie();
    products.push(id);
    setCookie("products", products);
    document.getElementById("putIntoCart").style.visibility = "visible";
}

function removeProductFromCart(entryNumber) {
    let products = getProductsFromCookie();
    let index = 0;
    products = products.filter(product => index++ != entryNumber);
    setCookie("products", products)
}