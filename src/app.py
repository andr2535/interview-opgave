from flask import Flask, jsonify, json
from flask_cors import CORS
from python_files.products import getProduct, getAllProducts

app = Flask(__name__)
CORS(app)


@app.route('/getAllProducts', methods=['GET'])
def getAllProductsJson():
    return getAllProducts()

@app.route("/productJson/<id>")
def productJson(id):
    product = getProduct(id)
    return jsonify(name=product.name, id=product.id, price=product.price, imgurl=product.imgurl)
