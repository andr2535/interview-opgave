
export class Product {
	public name: string;
	public price: number;
	public id: number;
	public imgurl: string;


	constructor(name:string, price:number, id:number, imgurl:string) {
		this.name = name;
		this.price = price;
		this.id = id;
		this.imgurl = imgurl
	}
}