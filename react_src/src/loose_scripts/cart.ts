// Code for manipulating the cart of the shop. Used by both the cart and the product page.
export function getCookies(): Map<string, string> {
    let cookieMap = new Map();
    document.cookie.split(';').forEach(cookieString => {
        let cookieArray = cookieString.split("=");
        cookieMap.set(cookieArray[0], cookieArray[1]);
	});
    return cookieMap;
}
export function setCookie(key:string, value:any) {
    document.cookie = key + "=" + JSON.stringify(value) + "; Path=/;"
}

export function getProductIdsFromCookie():Array<number> {
	const cookies = getCookies();
	const products = cookies.get("products");
    if (products) {
        return JSON.parse(products);
    }
    else {return [];}
}

export function addProductToCart(id: number) {
    let products = getProductIdsFromCookie();
    products.push(id);
    setCookie("products", products);
}

export function removeProductFromCart(entryNumber: number) {
    let products = getProductIdsFromCookie();
    let index = 0;
    products = products.filter(product => index++ !== entryNumber);
    setCookie("products", products)
}