import React from 'react';
import * as Constants from '../constants'
import {Product} from '../models/product';
import {addProductToCart} from '../loose_scripts/cart';

interface State {
  product?: Product;
  putIntoCart: boolean;
}

export class ProductComponent extends React.Component<{}, State> {
  props: any;
  
  constructor(props: any) {
    super(props);
    this.state = {
      product: undefined,
      putIntoCart: false
    };
  }
  
  async fetchProduct(id:number) {
    if (this.state.product) {
      return;
    }
    
    const product: Product = await (await fetch(`${Constants.constants.ApiHost}productJson/${id}`)).json();
    this.setState({"product": product, "putIntoCart": this.state.putIntoCart});
  }
  addToCart() {
    if (this.state.product) {
      addProductToCart(this.state.product.id);
      this.setState({"product": this.state.product, "putIntoCart": true});
    }
  }
  
  render() {
    const {id} = this.props;
    this.fetchProduct(id);
    return (
      <div>
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
          <div className="container">
            <div className="navbar-brand">
              <a href="/">Testshop</a>
              <a href="/cart">indkøbsvogn</a>
            </div>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
          </div>
        </nav>
  
        <div className="container">
          {
            this.state.product && 
            <div>
              <div className="row">
                Her vises produkt med id: {this.state.product.id}
              </div>
              <div>Produktet koster {this.state.product.price}</div>
              <a onClick={this.addToCart.bind(this)}>Læg i kurv</a>
              {this.state.putIntoCart && <div>Varen er lagt i kurven</div>}
            </div>
          }
        </div>
      </div>
      );
    }
}