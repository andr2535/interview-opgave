import React from 'react';
import {getProductIdsFromCookie, removeProductFromCart} from '../loose_scripts/cart';
import * as Constants from '../constants'
import {Product} from '../models/product';

interface State {
  products?: Array<Product>
}

export class Cart extends React.Component<{}, State> {
  productDivs:Array<any> = [];
  
  constructor(props: any) {
    super(props);
    this.state = {
      products: undefined
    };
  }
  
  removeProduct(index: number) {
    removeProductFromCart(index);
    this.setState({products: undefined});
  }

  async renderProducts() {
    if (this.state.products) {return;}
    const productIds = getProductIdsFromCookie();
    
    // Fetch real products from server, using their IDs from the cookie.
    let products:Array<Product> = await Promise.all(productIds.map(async function(id) {
      return await (await fetch(`${Constants.constants.ApiHost}productJson/${id}`)).json()
    }));

    let key = 0;
    this.productDivs = products.map(product => {
      const usedKey = key++;
      return (<div key={usedKey}>{product.name} med pris {product.price}: <a onClick={() => this.removeProduct(usedKey)}>Slet</a></div>);;
    });

    this.setState({products: products});
  }

  render() {
    this.renderProducts();
    return (
      <div>
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
          <div className="container">
            <div className="navbar-brand">
              <a href="/">Testshop</a>
              <a href="/cart">indkøbsvogn</a>
            </div>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
          </div>
        </nav>
  
        <div className="container">
          <div className="row">
            Indkøbsvogn
          </div>
          {this.productDivs}
        </div>
      </div>
      );
    }
}