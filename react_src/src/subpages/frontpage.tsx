import React, {useState} from 'react';
import {Product} from '../models/product';
import * as Constants from '../constants';

interface State {
  productDivs: Array<any>,
  has_fetched_products: boolean
}

export class Frontpage extends React.Component<{}, State> {
  products: Array<Product> = [];
  searchString: string = "";
  constructor(props: any) {
    super(props);
    this.state = {
      productDivs: [],
      has_fetched_products: false
    };
  }

  async getProductDivs() {
    if (this.state.has_fetched_products) {return;}
    
    this.products = await (await fetch(`${Constants.constants.ApiHost}getAllProducts`)).json();
    this.products = this.products.filter(product => {
      return product.name.indexOf(this.searchString) !== -1;
    });
    let productDivs = [];
    for (const productIndex in this.products) {
      productDivs.push((
      <div key={productIndex}>
        <a href={`/product/${this.products[productIndex].id}`}>{this.products[productIndex].name}</a>
      </div>));
    }
    this.setState({"productDivs": productDivs, has_fetched_products:true});
  }
  updateSearchString(event:any) {
    this.searchString = event.target.value;
    this.setState({productDivs:[], has_fetched_products: false});
  }

  render() {
    this.getProductDivs();
    return (
      <div>
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
          <div className="container">
            <div className="navbar-brand">
              Testshop
              <a href="/cart">indkøbsvogn</a>
            </div>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
          </div>
        </nav>
  
        <div className="container">
          <div className="row">
            Velkommen! Søg herunder
          </div>
          <input type="text" onChange={(e) => this.updateSearchString(e)} ></input>
          <div>
            {this.state['productDivs']}
          </div>
        </div>
      </div>
      );
    }
}