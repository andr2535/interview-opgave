import React from 'react';
import './App.css';
import { Route, BrowserRouter as Router } from 'react-router-dom'
import {Frontpage} from './subpages/frontpage';
import {ProductComponent} from './subpages/product';
import {Cart} from './subpages/cart';
function App() {
  const routs = (
    <Router>
       <div>
          <Route exact path="/" component={Frontpage} />
          <Route path="/product/:id" render= {({match}) =>( <ProductComponent id={match.params.id} />)} />
          <Route path="/cart/" component={Cart}/>
       </div>
    </Router>
  );
  return routs
}
export default App;
